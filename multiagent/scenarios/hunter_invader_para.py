import numpy as np
def Unit(m):
    return m*0.05

class ScenePara(object):
    discrete_action = True
    max_steps_episode = 300
    arena_size = Unit(m = 80)   # 
    hunter_num = 5
    invader_num = 2
    num_landmarks = 1

    num_agent = hunter_num + invader_num
    nest_center_pos = np.array([Unit(m=20),Unit(m=0)])
    hunter_spawn_pos_lim = Unit(m=20)    #!

    invader_spawn_limit = Unit(m=60)
    distance_dectection = Unit(m=25)
    intercept_hunter_needed = 3 # 需要3个围攻，才能实现拦截

    hunter_affect_range = Unit(m=4)  # hunter affect range 4m
    hunter_speed_pressure = Unit(m=4)   # 每一个hunter造成4m/s的减速
    visualize = False

    Invader_Size = Unit(m=2)    # 2m radius
    Invader_Accel = Unit(m=700)  # 700 m/s^2 , that's 70G ????
    Invader_MaxSpeed = Unit(m=20)  # 20 m/s , that's 70G ????
    
    Hunter_Size = Unit(m=1.5)   # 1.5m radius
    Hunter_Accel = Unit(m=600)  # 600 m/s^2 , that's 70G ????
    Hunter_MaxSpeed = Unit(m=16)  # 16 m/s , that's 70G ????

    Landmark_Size = Unit(m = 5)
    Invader_Kill_Range = Unit(m=5)  # 和Landmark_Size设置为一致吧

