import numpy as np
from multiagent.core import World, Agent, Landmark
from multiagent.scenario import BaseScenario


class Scenario(BaseScenario):
    def __init__(self, num_agents=3, dist_threshold=0.1, arena_size=1, identity_size=0):
        self.num_agents = num_agents
        #self.rewards = np.zeros(self.num_agents)
        #self.temp_done = False
        self.dist_threshold = dist_threshold
        self.arena_size = arena_size
        self.identity_size = identity_size
        self.flag = [False,False]
        self.count_1 = 0
        self.count_2 = 0
    def make_world(self):
        world = World()
        # set any world properties first
        world.dim_c = 2
        num_good_agents = 2
        num_adversaries = 5
        num_agents = num_adversaries + num_good_agents
        num_landmarks = 2
        # add agents
        world.agents = [Agent() for i in range(num_agents)]
        for i, agent in enumerate(world.agents):
            agent.name = 'agent %d' % i
            agent.collide = True
            agent.silent = True
            #agent.adversary = True if i < num_adversaries else False
            agent.adversary = True if i < num_adversaries else False
            agent.size = 0.075 if agent.adversary else 0.05
            #agent.accel = 3.0 if agent.adversary else 4.0
            agent.accel = 3 if agent.adversary else 4
            #agent.accel = 20.0 if agent.adversary else 25.0
            #agent.max_speed = 1.0 if agent.adversary else 1.3
            agent.max_speed = 1.0 if agent.adversary else 1.3
            agent.indx = i
        # add landmarks
        world.landmarks = [Landmark() for i in range(num_landmarks)]
        for i, landmark in enumerate(world.landmarks):
            landmark.name = 'landmark %d' % i
            landmark.collide = True
            landmark.movable = False
            landmark.size = 0.2
            landmark.boundary = False
        # make initial conditions
        self.reset_world(world)
        return world


    def reset_world(self, world):
        # random properties for agents
        #print(world.steps)
        for i, agent in enumerate(world.agents):
            agent.color = np.array([0.35, 0.85, 0.35]) if not agent.adversary else np.array([0.85, 0.35, 0.35])
            # random properties for landmarks
        for i, landmark in enumerate(world.landmarks):
            landmark.color = np.array([0.25, 0.25, 0.25])
        # set random initial states
        for agent in world.agents:
            agent.state.p_pos = np.random.uniform(-self.arena_size, +self.arena_size, world.dim_p)
            agent.state.p_vel = np.zeros(world.dim_p)
            agent.state.c = np.zeros(world.dim_c)
        for i, landmark in enumerate(world.landmarks):
            if not landmark.boundary:
                landmark.state.p_pos = np.random.uniform(-0.8*self.arena_size, +0.8*self.arena_size, world.dim_p)
                landmark.state.p_vel = np.zeros(world.dim_p)
        self.flag = [False,False]
        self.count_1 = 0
        self.count_2 = 0
        world.steps = 0



    def benchmark_data(self, agent, world):
        # returns data for benchmarking purposes
        if agent.adversary:
            collisions = 0
            for a in self.good_agents(world):
                if self.is_collision(a, agent):
                    collisions += 1
            return collisions
        else:
            return 0


    def is_collision(self, agent1, agent2):
        delta_pos = agent1.state.p_pos - agent2.state.p_pos
        dist = np.sqrt(np.sum(np.square(delta_pos)))
        dist_min = agent1.size + agent2.size
        return True if dist < dist_min else False

    # return all agents that are not adversaries
    def good_agents(self, world):
        return [agent for agent in world.agents if not agent.adversary]

    # return all adversarial agents
    def adversaries(self, world):
        return [agent for agent in world.agents if agent.adversary]


    def reward(self, agent, world):
        # Agents are rewarded based on minimum agent distance to each landmark
        main_reward = self.adversary_reward(agent, world) if agent.adversary else self.agent_reward(agent, world)
        return main_reward

    def agent_reward(self, agent, world):
        # Agents are negatively rewarded if caught by adversaries
        rew = 0
        shape = True
        adversaries = self.adversaries(world)
        if shape:  # reward can optionally be shaped (increased reward for increased distance from adversary)
            if self.flag[agent.indx-5] == True:
                rew += 0
            if self.flag[agent.indx-5] == False:
               for adv in adversaries:
                   rew += 0.05 * np.sqrt(np.sum(np.square(agent.state.p_pos - adv.state.p_pos)))
        if agent.collide:
            for a in adversaries:
                if self.is_collision(a, agent):
                    rew -= 10
                    #if self.flag[agent.indx-5] == False:
                     #  self.flag[agent.indx-5] = True
                    #if self.flag[agent.indx-5] == True:
                     #  rew -= 0
                    #rew -= 10

        # agents are penalized for exiting the screen, so that they can be caught by the adversaries
        def bound(x):
            if x < 0.9:
                return 0
            if x < 1.0:
                return (x - 0.9) * 10
            return min(np.exp(2 * x - 2), 10)
        for p in range(world.dim_p):
            x = abs(agent.state.p_pos[p])
            rew -= bound(x)
        #print(self.flag,"good")

        return rew

    def adversary_reward(self, agent, world):
        # Adversaries are rewarded for collisions with agents
        rew = 0
        shape = True
        agents = self.good_agents(world)
        adversaries = self.adversaries(world)
        if shape:  # reward can optionally be shaped (decreased reward for increased distance from agents)
           for good in agents:
               if self.flag[good.indx-5] == True:
                  rew -= 0
               if self.flag[good.indx-5] == False:
                  for adv in adversaries:
                      rew -= 0.1 * min([np.sqrt(np.sum(np.square(a.state.p_pos - adv.state.p_pos))) for a in agents])
                      #rew -= 0.1 * np.sqrt(np.sum(np.square(good.state.p_pos - adv.state.p_pos)))
            #for adv in adversaries:
            #    rew -= 0.05 * min([np.sqrt(np.sum(np.square(a.state.p_pos - adv.state.p_pos))) for a in agents])
        if agent.collide:
            for ag in agents:
                for adv in adversaries:
                    if self.is_collision(ag, adv):
                       #rew += 10    v1
                       if ag.indx == 5:
                          if self.flag[0] == False and self.flag[1] == False:
                             rew += 10      # v2
                             self.flag[0] = True
                             self.count_1 += 1    #v2
                          if self.flag[0] == True and self.flag[1] == False:
                             self.count_1 += 1    #v2
                             if self.count_1 >2:  #v2
                                rew -= 0
                          if self.flag[0] == False and self.flag[1] == True:
                             rew += 20
                             self.flag[0] = True
                       if ag.indx == 6:
                          if self.flag[1] == False and self.flag[0] == False:
                             rew += 10
                             self.flag[1] = True
                             self.count_2 += 1
                          if self.flag[1] == True and self.flag[0] == False:
                             self.count_2 += 1
                             if self.count_2 > 2:
                                rew -= 0
                          if self.flag[1] == False and self.flag[0] == True:
                             rew += 20
                             self.flag[1] = True

                       #if self.flag[ag.indx-5] == False:
                        #  rew += 15
                         # self.flag[ag.indx-5] = True
                       #if self.flag[ag.indx-5] == True:
                        #  rew += 0
                       #if ag.indx = 5:
                        #  if self.flag[]
        """
        def bound(x):
            if x < 0.9:
                return 0
            if x < 1.0:
                return (x - 0.9) * 10
            return min(np.exp(2 * x - 2), 10)
        for p in range(world.dim_p):
            x = abs(agent.state.p_pos[p])
            rew -= bound(x)
        """
        #print(self.flag,"adv")

        return rew

    def observation(self, agent, world):
        # get positions of all entities in this agent's reference frame
        entity_pos = []
        #print(type(agent.state.p_vel))
        for entity in world.landmarks:
            if not entity.boundary:
                entity_pos.append(entity.state.p_pos - agent.state.p_pos)
        # communication of all other agents
        comm = []
        other_pos = []
        other_vel = []
        if agent.adversary:
           for other in world.agents:
               if other is agent: continue
               comm.append(other.state.c)
               if not other.adversary:
                  other_pos.append(other.state.p_pos - agent.state.p_pos)
            #if not other.adversary:
                  other_vel.append(other.state.p_vel)
        if not agent.adversary:
           for other in world.agents:
               if other is agent: continue
               comm.append(other.state.c)
               #if not other.adversary:
               other_pos.append(other.state.p_pos - agent.state.p_pos)
            #if not other.adversary:
               other_vel.append(other.state.p_vel)
        #return np.concatenate([agent.state.p_vel] + [agent.state.p_pos] + entity_pos)
        if agent.adversary:
           xx = [0,0]
           xx = np.array(xx)
           default_obs = np.concatenate([agent.state.p_vel] + [agent.state.p_pos] + entity_pos + other_pos + other_vel + [np.array([0,agent.indx])] + [xx] + [xx] + [xx] + [xx] + [xx] + [xx] + [xx] + [xx])
           #print(default_obs.shape)
        if not agent.adversary:
           default_obs = np.concatenate([agent.state.p_vel] + [agent.state.p_pos] + entity_pos + other_pos + other_vel + [np.array([0,agent.indx])])
        #default_obs = np.concatenate([agent.state.p_vel] + [agent.state.p_pos] + entity_pos + [np.array([0,agent.indx])])
        #default_obs = default_obs.tolist()
        #print(default_obs.shape)
        #print(default_obs.shape)
        return default_obs

    def done(self, agent, world):
        condition1 = world.steps >= world.max_steps_episode
        #print('fuck')
        success_list = []
        agents = self.good_agents(world)
        adversaries = self.adversaries(world)
        for adv in adversaries:
            for a in agents:
                if np.sqrt(np.sum(np.square(a.state.p_pos - adv.state.p_pos))) < 0.125:
                    success_list.append(True)
                    #flag[i] = True
        success_list = np.array(success_list)
        #self.is_success = np.any(success_list == True)
        #print(self.is_success)
        if self.flag[0] == True and self.flag[1] == True:
            self.is_success = True
        else:
            self.is_success = False
        #self.is_success = np.all(self.flag == True)
        #self.is_success = np.all(self.min_dists < world.dist_thres)
        return condition1 or self.is_success

    def info(self, agent, world):
        return {'is_success': self.is_success, 'world_steps': world.steps}
                #'reward':self.joint_reward, 'dists':self.delta_dists.mean()}
