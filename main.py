import os
import json
import datetime
import numpy as np
import torch
import utils
import random
from copy import deepcopy
from arguments import get_args
from tensorboardX import SummaryWriter
from eval import evaluate
from learner import Get_ENV_Learner
from pprint import pprint
if __name__ == '__main__':
    # communicate with plot server on MATLAB via files or UDP
    from mCOMv4 import mUDP_client

def print红(*kw):
    print("\033[0;31m",*kw,"\033[0m")
def print绿(*kw):
    print("\033[0;32m",*kw,"\033[0m")
def print黄(*kw):
    print("\033[0;33m",*kw,"\033[0m")
def print蓝(*kw):
    print("\033[0;34m",*kw,"\033[0m")
def print紫(*kw):
    print("\033[0;35m",*kw,"\033[0m")
def print靛(*kw):
    print("\033[0;36m",*kw,"\033[0m")

np.set_printoptions(suppress=True, precision=4)


def train(args, return_early=False):
    # training reveal
    uc = mUDP_client(path = "/home/qingxu/Desktop/hunter_invader/", digit=6)
    uc.rec_init()
    writer = SummaryWriter(args.log_dir)
    envs = utils.make_parallel_envs(args)
    ENV_Learner = Get_ENV_Learner(args)
    # used during evaluation only
    eval_master, eval_env = Get_ENV_Learner(args, return_env=True)
    obs = envs.reset() # shape - num_processes x num_agents x obs_dim
    ENV_Learner.initialize_obs(obs)
    n = len(ENV_Learner.all_agents)
    episode_rewards = torch.zeros([args.num_processes, n], device=args.device)
    final_rewards = torch.zeros([args.num_processes, n], device=args.device)

    # start simulations
    start = datetime.datetime.now()
    #print(args.num_updates)
    for j in range(args.num_updates):
        for step in range(args.num_steps):
            #print(args.num_steps)
            with torch.no_grad():
                actions_list = ENV_Learner.act(step)
            agent_actions = np.transpose(np.array(actions_list),(1,0,2))
            obs, reward, done, info = envs.step(agent_actions, show = False)
            reward = torch.from_numpy(np.stack(reward)).float().to(args.device)
            episode_rewards += reward
            masks = torch.FloatTensor(1-1.0*done).to(args.device)
            final_rewards *= masks
            final_rewards += (1 - masks) * episode_rewards
            episode_rewards *= masks

            ENV_Learner.update_rollout(obs, reward, masks)

        ENV_Learner.wrap_horizon()
        return_vals = ENV_Learner.update()
        value_loss = return_vals[:, 0]
        action_loss = return_vals[:, 1]
        dist_entropy = return_vals[:, 2]
        ENV_Learner.after_update()

        if j%args.save_interval == 0 and not args.test:
            savedict = {'models': [agent.actor_critic.state_dict() for agent in ENV_Learner.all_agents]  }
            ob_rms = (None, None) if envs.ob_rms is None else (envs.ob_rms[0].mean, envs.ob_rms[0].var)
            savedict['ob_rms'] = ob_rms
            savedir = args.save_dir+'/ep'+str(j)+'.pt'
            torch.save(savedict, savedir)

        total_num_steps = (j + 1) * args.num_processes * args.num_steps

        if j%args.log_interval == 0:
            end = datetime.datetime.now()
            seconds = (end-start).total_seconds()
            mean_reward = final_rewards.mean(dim=0).cpu().numpy()
            assert len(mean_reward) == 7 # modify if for different scene
            uc.rec(np.mean(mean_reward[0:2]), 'invader mean reward')
            uc.rec(np.mean(mean_reward[3:7]), 'hunter mean reward')

            uc.rec_show()
            print("Updates {} | Num timesteps {} | Time {} | FPS {}\nMean reward {}\nEntropy {:.4f} Value loss {:.4f} Policy loss {:.4f}\n".
                  format(j, total_num_steps, str(end-start), int(total_num_steps / seconds),
                  mean_reward, dist_entropy[0], value_loss[0], action_loss[0]))
            if not args.test:
                for idx in range(n):
                    writer.add_scalar('agent'+str(idx)+'/training_reward', mean_reward[idx], j)

                writer.add_scalar('all/value_loss', value_loss[0], j)
                writer.add_scalar('all/action_loss', action_loss[0], j)
                writer.add_scalar('all/dist_entropy', dist_entropy[0], j)

        if args.eval_interval is not None and j%args.eval_interval==0:
            ob_rms = (None, None) if envs.ob_rms is None else (envs.ob_rms[0].mean, envs.ob_rms[0].var)
            print('===========================================================================================')
            _, eval_perstep_rewards, final_min_dists, num_success, eval_episode_len = evaluate(args, None, ENV_Learner.all_policies,
                                                                                               ob_rms=ob_rms, env=eval_env,
                                                                                               learner=eval_master, render=args.render)
            print('Evaluation {:d} | Mean per-step reward {:.2f}'.format(j//args.eval_interval, eval_perstep_rewards.mean()))
            print('Num success {:d}/{:d} | Episode Length {:.2f}'.format(num_success, args.num_eval_episodes, eval_episode_len))
            if final_min_dists:
                print('Final_dists_mean {}'.format(np.stack(final_min_dists).mean(0)))
                print('Final_dists_var {}'.format(np.stack(final_min_dists).var(0)))
            print('===========================================================================================\n')

            if not args.test:
                writer.add_scalar('all/eval_success', 100.0*num_success/args.num_eval_episodes, j)
                writer.add_scalar('all/episode_length', eval_episode_len, j)
                for idx in range(n):
                    writer.add_scalar('agent'+str(idx)+'/eval_per_step_reward', eval_perstep_rewards.mean(0)[idx], j)
                    if final_min_dists:
                        writer.add_scalar('agent'+str(idx)+'/eval_min_dist', np.stack(final_min_dists).mean(0)[idx], j)

            curriculum_success_thres = 0.9
            if return_early and num_success*1./args.num_eval_episodes > curriculum_success_thres:
                savedict = {'models': [agent.actor_critic.state_dict() for agent in ENV_Learner.all_agents]}
                ob_rms = (None, None) if envs.ob_rms is None else (envs.ob_rms[0].mean, envs.ob_rms[0].var)
                savedict['ob_rms'] = ob_rms
                savedir = args.save_dir+'/ep'+str(j)+'.pt'
                torch.save(savedict, savedir)
                print('===========================================================================================\n')
                print('{} agents: training complete. Breaking.\n'.format(args.num_agents))
                print('===========================================================================================\n')
                break

    writer.close()
    print("all job finished")
    if return_early:
        return savedir

if __name__ == '__main__':
    args = get_args()

    # 处理模型路径的问题
    print紫("模型保存路径为:"+args.save_dir)
    if not args.test:
        if os.path.exists(args.save_dir):
            print黄("模型保存路径已经存在！os.rename(args.save_dir, args.save_dir+'_old') ！")
            os.rename(args.save_dir, args.save_dir+'_old')
        print黄("模型保存路径不存在，创建路径！")
        os.makedirs(args.save_dir)


    if args.seed is None:
        args.seed = random.randint(0,10000)
    args.num_updates = args.num_frames // args.num_steps // args.num_processes
    torch.manual_seed(args.seed)
    torch.set_num_threads(18)
    np.random.seed(args.seed)
    if args.cuda:
        torch.cuda.manual_seed(args.seed)

    pprint(vars(args))
    if not args.test:
        with open(os.path.join(args.save_dir, 'params.json'), 'w') as f:
            params = deepcopy(vars(args))
            params.pop('device')
            json.dump(params, f)
    train(args)
